/*****************************************************************************/
/* Play: Event Handlers and Helpersss .js*/
/*****************************************************************************/
Template.Play.events({

    'click #playerResponseButton': function (event, template) { //This helped me: http://stackoverflow.com/questions/28034512/dynamic-radio-button-creation-using-handlebars-in-meteor
        event.preventDefault();
        var playerResponseCode = $(":radio[name=playerResponsesRadio]:checked").val();
        console.log('playerResponseCode: ' + playerResponseCode);

        Meteor.call('/app/processPlay', Session.get('SCENARIO'), Session.get('NPCHARACTER'), Session.get('NPCHARACTER_REQUEST'), Session.get('PLAYER_RESPONSES'), Session.get('PLAY_RESULT'), Session.get('BASE'), Session.get('HIT'), playerResponseCode, function (err, response) {
            console.log(JSON.stringify(response));
            Session.set('result', JSON.stringify(response));

            Session.set('WAS_HIT', response.wasHit);

            var score = 0;
            var base = 0;

            if (Meteor.user().profile.score != undefined) {
                score = Meteor.user().profile.score;

            }
            if (Meteor.user().profile.base != undefined) {
                base = Meteor.user().profile.base;

            }
            Session.set('USER_SCORE', score);

            if (response.playResult != null) {

                alert(response.playResult);
            }

            // response.helloWord();
            if (response.newScenario != null) {
                Template.Play.setScenario(Session.get('SCENARIO'), response.newNPCharacter, response.newScenario);
            }

            if (response.newNPCharacter != null) {
                Template.Play.setScenario(Session.get('NPCHARACTER'), response.newNPCharacter);
            }

            // if new scenario, set new scenario
            // if new npCharacterRequest, set new npCharacterRequest
            // newScenario:null,newNPCharacter:null,newNPCharacterRequest:null
            if (response.newNPCharacterRequest != null) {
                Template.Play.setScenario(Session.get('SCENARIO'), Session.get('NPCHARACTER_REQUEST'), response.newNPCharacterRequest);
            }

            if (response.newPlayerResponse != null) {
                Template.Play.setScenario(Session.get('SCENARIO'), Session.get('PLAYER_RESPONSE'), response.newPlayerResponse);
            }

            if (response.randomScenario === true) {

                Template.Play.randomScenario();
            }

            if (err) {
                alert(err);
            }
        });
    }
});

Template.Play.helpers({
    scenario: function () {

        var scenario = Session.get('SCENARIO');
        return scenario;
    },
    npCharacter: function () {

        var character = Session.get('NPCHARACTER');
        return character;
    },
    npCharacterRequest: function () {

        var request = Session.get('NPCHARACTER_REQUEST');
        return request;

    },
    playerResponses: function () {

        var responses = Session.get('PLAYER_RESPONSES');
        return responses;
    },
    userScore: function () {

        return Session.get('USER_SCORE');

    },
    playResult: function () {

        return Session.get('PLAY_RESULT');

    },
    base: function () {

        return Meteor.user().profile.base;
    }
});

// Basically as the game plays
// There's going to be a counter
// The counter increments per interaction
// The counter will determine who you talk to
// And what you talk about
// Potentially each NPCharacter will have their own counter
// That way if you encounter the same NPCharacter, it will choose the next thing.
// The objective is to steal everything from the store that you can while not looking suspicious
// Talk to NPC's to lower suspicion.
// Opportunity's to steal are random and will move you to the next part of the store until you enter the checkout line
// The check out line will direct you to the cop and he will either arrest you or not.


Template.Play.randomScenario = function () {
    // Random Scenario
    //This line is fine, it's finding all of the Scenarios
    //var wasHit;
    // wasHit = Play.helpers.playResult();

    var scenarios = Scenario.find({}).fetch();


    var hit;
    hit = Session.get('WAS_HIT');

    var base = Meteor.user().profile.base;
    if(base === undefined) {
        base = 0;
    }
    if (hit === true) {
        base++;
    } else {
        base = 0;
    }

//  var randomIndex = 2; //Math.floor ((Math.random () * scenarios.length));

    var gameScenario = scenarios[base];


    console.log('gameScenario: ' + JSON.stringify(gameScenario));

    // Random Character based on found scenario
    var npCharacters = NPCharacter.find({scenarioCodes: gameScenario.objectCode}).fetch();

    console.log('npCharacters: ' + JSON.stringify(npCharacters));

    //var randomIndex = 0; //Math.floor ((Math.random () * npCharacters.length));

    // Character is chosen based on above^^


    var randomCharacter = npCharacters[0];
    Session.set('BASE', base);
    Meteor.users.update({_id: Meteor.user()._id}, {$set: {"profile.base": base}});
    if ( base === 4) {
        var score = Session.get('USER_SCORE');
        score++;
        Session.set('USER_SCORE', score);
        base = 0;
        Session.set('BASE', base);
        Meteor.users.update({_id: Meteor.user()._id}, {$set: {"profile.score": score}});
        Meteor.users.update({_id: Meteor.user()._id}, {$set: {"profile.base": base}});

    }
    /* if (wasHit) {
     characterIndex ++;
     }
     if (!wasHit) {
     characterIndex = 0;
     }
     */
    // Character is created
    console.log('randomCharacter: ' + JSON.stringify(randomCharacter));

    // NPCharacter's request pool is generated
    var requests = NPCharacterRequest.find({objectCode: {$in: randomCharacter.npCharacterRequestCodes}}).fetch();

    console.log('requests: ' + JSON.stringify(requests));

    //var randomIndex = 0; //Math.floor ((Math.random () * requests.length));

    // NPCharacter's request is generated from the pool
    var requestIndex;
    if (requestIndex === undefined) {
        requestIndex = 0;
    }
    if (hit === true) {
        requestIndex++;
    } else {
        requestIndex = 0;
    }
    var randomRequest = requests[0];

    /* if (!wasHit) {
     requestIndex = 0;
     }

     if (firstTime) {
     requestIndex++;
     firstTime = false;
     }
     */
    // NPCharacter says words
    console.log('randomRequest: ' + JSON.stringify(randomRequest));


    var playerResponse = PlayerResponse.find({objectCode: randomRequest.objectCode}).fetch();

    // Scenario is put together based on generated things from above
    Template.Play.setScenario(gameScenario, npCharacters, randomRequest, playerResponse);

};

Template.Play.setScenario = function (scenario, character, NPCRequest, pResponse) {

    Session.set('SCENARIO', scenario);
    Session.set('NPCHARACTER', character);
    Session.set('NPCHARACTER_REQUEST', NPCRequest);
    Session.set('PLAYER_RESPONSES', pResponse);

    console.log('playerResponses: ' + JSON.stringify(pResponse));
};

/*****************************************************************************/
/* Play: Lifecycle Hooks */
/*****************************************************************************/
Template.Play.created = function () {

    var score = 0;
    var base = 0;

    if (Meteor.user().profile.score != undefined) {
        score = Meteor.user().profile.score;

    }
    if (Meteor.user().profile.base != undefined) {
        base = Meteor.user().profile.base;

    }
    Session.set('USER_SCORE', score);
    Session.set('BASE', base);

    Template.Play.randomScenario();

};

Template.Play.rendered = function () {


};

Template.Play.destroyed = function () {
};