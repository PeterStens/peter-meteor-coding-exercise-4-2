/*****************************************************************************/
/* ProcessPlay Methods */
/*****************************************************************************/

Meteor.methods({
    '/app/processPlay': function (scenario, npCharacter, npCharacterRequest, playerResponseCode, player_response, wasHit) {

        var user = Meteor.user();

        var score = user.profile.score;

        var base = user.profile.base;

        if (score === undefined) {

            score = 0;
        }
        var firstTime;
        firstTime = true;

        if (firstTime === true) {
            base = 0;
            firstTime = false;
        }

        var hitMessage;

        // Evaluate input elements
        // In this case, results are completely random
        var randomResult = Math.random() < 0.5 ? true : false; // http://stackoverflow.com/questions/9730966/how-to-decide-between-two-numbers-randomly-using-javascript

        //var setResult = true;

        if (randomResult === true) {
            hitMessage = "You hit it!";
            base++;
            // If necessary increment user's score
        } else {
            hitMessage = "You miss!";
            base = 0;
        }
        if (base === 4) {
            score++;
            base = 0;
        }

        // If necessary select a
        // new scenario,
        var scenarios = Scenario.find({}).fetch();
        var scenariosNumber = base;
        var newScenario = scenarios[scenariosNumber];
// character,
        var npCharacters = NPCharacter.find({}).fetch ();
        var characterNumber = 0;
        if (randomResult === true)  {
            characterNumber++;
        }
        var newNPCharacter = npCharacters[characterNumber];

        //new player request
       /* var playerResponse = playerResponseCode.find({}).fetch ();
        var responseNumber = 0;
        if (randomResult === true)  {
            responseNumber++;
        }
        var newPlayerResponse = playerResponse[responseNumber];
        */
        // and request,
        //var requests = NPCharacterRequest.find ({objectCode: {$in: randomCharacter.npCharacterRequestCodes}}).fetch ();

        // and return them to the template


        Meteor.users.update({_id: Meteor.user()._id}, {$set: {"profile.score": score}}, {$set: {"profile.base": base}});

        var response = {};
        response.randomScenario = true;
        response.playResult = hitMessage;
        response.newScenario = newScenario;
        response.newNPCharacter = newNPCharacter;
        response.newNPCharacterRequest = true;
        response.newPlayerResponse = true;
        response.wasHit = randomResult;
        response.base = base;

       /* response.helloWord = function () {
            console.log('Hello World');
        };

        response.helloWord();
        */

        return response;
    }
});