/*
 * Add query methods like this:
 *  PlayerRequest.findPublic = function () {
 *    return PlayerRequest.find({is_public: true});
 *  }
 */
PlayerRequest.allow({
  insert: function (userId, doc) {
    return true;
  },

  update: function (userId, doc, fieldNames, modifier) {
    return true;
  },

  remove: function (userId, doc) {
    return true;
  }
});

PlayerRequest.deny({
  insert: function (userId, doc) {
    return false;
  },

  update: function (userId, doc, fieldNames, modifier) {
    return false;
  },

  remove: function (userId, doc) {
    return false;
  }
});